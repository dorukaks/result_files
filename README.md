# README #

Welcome to the repository for trained networks of the paper "Inverse Design of Self-Oscillating Gels Through Deep Learning".
This repository has the results for the statistics of the trained networks for the publication. The inverse design architecture mainly relies on PyTorch machine learning library (more dependencies can be found in the respective files).


### Contents ###

* Classification
* Regression

### Classification ###
Statistics of the PyTorch based classifier ["CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py"](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/Regression/CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py) are stored here. The name of the files are in "PCA_pytorch_[NumberofInstances]instance_[NumberofLayers]layers_[HiddenUnits].pckl"

* NumberofInstances: Number of simulation snapshots used in training
* NumberofLayers: Number of hidden layers in the MLP classifier
* HiddenUnits: Number of hidden units in each hidden layer in the MLP classifier

### Regression ###
Statistics of the PyTorch based regressor ["CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py"](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/Regression/CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py) are stored here. The name of the files are in "PCA_pytorch_[NumberofInstances]instance_split_[NumberofLayers]layers_[HiddenUnits].pckl"

* NumberofInstances: Number of simulation snapshots used in training
* NumberofLayers: Number of hidden layers in the MLP classifier
* HiddenUnits: Number of hidden units in each hidden layer in the MLP classifier

### Related Repositories ###

* [Main Files](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/) - Main files and libraries for training the networks can be found in this repository 

* [Trained Networks](https://bitbucket.org/dorukaks/trained_networks/src/main/) - The networks trained using the presented architecture can be found here. The networks and the auxillary preprocessors (such as PCA and scalers) are packed/saved using PyTorch module.



* For further questions regarding this work, please contact to owner of this repository  (Doruk Aksoy)
* [Computational Autonomy Group](http://www.alexgorodetsky.com/index.html)



Copyright © 2020-2021 Doruk Aksoy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
